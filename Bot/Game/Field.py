# -*- coding: utf-8 -*-
# Python3.4*

import copy


class Field:
    def __init__(self):
        self.width = 10;
        self.height = 20;
        self.field = [[0]*self.width]*self.height;

    def size(self):
        return self.width, self.height

    def updateField(self, field):
        self.field = field

    @staticmethod
    def projectPieceDown(field, pieceP, offset, heightMap):
        piecePositions = Field.offsetPiece(pieceP, offset)

        minYdist = 99;
        for p in piecePositions:

            if p[0] >= len(heightMap):
                return None;


            if 20-p[1]-1-heightMap[p[0]] < minYdist:
                minYdist = 20-p[1]-1-heightMap[p[0]];


        newfield = Field.fitPiece(field, piecePositions, [0, minYdist])
        #newfield = field;

        '''
        for height in range(0, 19):
            tmp = Field.fitPiece(field, piecePositions, [0, height])

            if not tmp:
                break
            newfield = tmp

        '''

        return newfield

    @staticmethod
    def offsetPiece(piecePositions, offset):
        piece = copy.deepcopy(piecePositions)
        for pos in piece:
            pos[0] += offset[0]
            pos[1] += offset[1]

        return piece

    @staticmethod
    def checkIfPieceFits(field, piecePositions):
        for x, y in piecePositions:
            if 0 <= x < 10 and -1 <= y < 20:
                if y >= 0 and field[y][x] > 1:
                    3/0
                    return False
            else:
                return False;
        return True

    @staticmethod
    def fitPiece(field, piecePositions, offset=None):
        if offset:
            piece = Field.offsetPiece(piecePositions, offset)
        else:
            piece = piecePositions

        field = copy.deepcopy(field)
        if Field.checkIfPieceFits(field, piece):
            for x, y in piece:
                if y >= 0:
                    field[y][x] = 4

            return field
        else:
            return None
