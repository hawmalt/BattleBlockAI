# -*- coding: utf-8 -*-
# Python3.4*

from random import randint
from Bot.Strategies.AbstractStrategy import AbstractStrategy


class RandomStrategy(AbstractStrategy):

    def __init__(self, game):
        AbstractStrategy.__init__(self, game)
        self._actions = ['left', 'right', 'turnleft', 'turnright', 'down', 'drop']
        self._actionProbability = [];

    def getLottery(self):
        #self._game

        #probablilityDistribution = [];
        lottery = [];
        lottery.append('left');
        lottery.append('left');
        lottery.append('left');
        lottery.append('left');
        lottery.append('right');
        lottery.append('turnleft');
        lottery.append('turnright');
        lottery.append('down');

        return lottery;


    def choose(self):
        lottery = self.getLottery();

        ind = [randint(0, len(lottery)-1) for _ in range(1, 10)];

        # moves = map(lambda x: self._actions[x], ind)
        # moves = list(map(lambda x: self._actions[x], ind))
        moves = [lottery[x] for x in ind];
        moves.append('drop');

        return moves;
