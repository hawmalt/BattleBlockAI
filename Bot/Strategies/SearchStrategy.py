# -*- coding: utf-8 -*-
# Python3.4*

from random import randint
from Bot.Strategies.AbstractStrategy import AbstractStrategy
from Bot.Game.Field import Field
import copy

class SearchStrategy(AbstractStrategy):
    def __init__(self, game):
        AbstractStrategy.__init__(self, game)
        self._actions = ['left', 'right', 'turnleft', 'turnright', 'down', 'drop']
        self.heightmap = [0]*10;

    def updateHeightMap(self, field):
        self.heightmap = [0]*10;
        for width in range(10):
            for height in range(20):
                if field[height][width] > 1:
                    self.heightmap[width] = 20-height;
                    break;

    def getReward(self, field, completedLinesScore):
        return -3000*(1-1/20*self.getHeight(field))*self.getHoles(field) + 700*completedLinesScore - 68*self.getBumpy(field) - 52*self.getHeight(field)*self.getHeight(field) - 347*self.getAntiTower(field) - 163*self.getWell();

    def getAntiTower(self, field):
        h = self.getHeight(field);


        bumpy = 0;
        for width in range(0, 10):
            bumpy = bumpy+(h-self.heightmap[width])*(h-self.heightmap[width])
        return bumpy;

    def getWell(self):
        well = 0;

        if self.heightmap[1] > self.heightmap[0]:
            well = well + (self.heightmap[1] - self.heightmap[0]) * (self.heightmap[1] - self.heightmap[0]);

        for width in range(1, 8):
            if (self.heightmap[width-1] > self.heightmap[width]) and (self.heightmap[width+1] > self.heightmap[width]):
                avg = (self.heightmap[width-1] + self.heightmap[width+1])/2
                well = well+((avg - self.heightmap[width])*(avg - self.heightmap[width]));
        return well;

    def getComplete(self, field):
        completeCount = [];
        newfield = copy.deepcopy(field);

        for height in range(0, 20):
            if self.isRow(newfield, height):
                completeCount.append(height);

        for row in completeCount:
            newfield.remove(newfield[height]);
            newfield.insert(0, [0]*10)


        return 3*int(self._game.me.combo) + len(completeCount), newfield;

    def isRow(self, field, row):
        for width in range(0, 10):
            if field[row][width] <= 1:
                return False;
        return True;

    def getHoles(self, field):
        numHoles = 0;
        for width in range(0, 10):
            sawBlock = False;
            for height in range(0, 20):
                if not sawBlock:
                    if field[height][width] > 1:
                        sawBlock = True;
                elif field[height][width] == 0:
                    numHoles = numHoles+(20-height);

        return numHoles;

    def getHeight(self, field):
        maxHeight = 0;
        for width in range(0, 10):
            if self.heightmap[width] > maxHeight:
                maxHeight = self.heightmap[width];


        return maxHeight;

    def getBumpy(self, field):
        h = self.getHeight(field)
        bumpy = 0;
        for width in range(0, 9):
            bumpy = bumpy+abs((self.heightmap[width+1]-self.heightmap[width])*(self.heightmap[width+1]-self.heightmap[width]))
        return bumpy;

    #returns -1 if the piece does not fit at first
    def getLeftDist(self, field, piecepos):
        dist = 0;

        piecefit = True;

        for p in piecepos:
            if p[0] < 0 or (p[1] >= 0 and field[p[1]][p[0]] > 1):
                piecefit = False;

        while piecefit:
            dist = dist + 1;
            piecepos = Field.offsetPiece(piecepos, [-1, 0]);
            for p in piecepos:
                if p[0] < 0 or (p[1] >= 0 and field[p[1]][p[0]] > 1):
                    piecefit = False;

        return dist - 1;

    def bfs(self, moves):
        offx = 0;
        rot = 0;

        offxNext = 0
        rotNext = 0

        bestOffx = 0;
        bestRot = 0;
        bestScore = -99999999;

        for _ in range(len(self._game.piece._rotations)):
            gamePiece = Field.offsetPiece(self._game.piece.positions(), self._game.piecePosition);
            offx = -self.getLeftDist(self._game.me.field.field, gamePiece);
            if offx > 0:
                self._game.piece.turnRight();
                rot = rot+1;
                continue;

            self.updateHeightMap(self._game.me.field.field);
            newField = Field.projectPieceDown(self._game.me.field.field, gamePiece, [offx, 0], self.heightmap);

            while newField != None:
                self.updateHeightMap(newField);
                countcompleteline, completeLineField = self.getComplete(newField);
                hasSeenLine = False

                '''
                curScore = self.getReward(completeLineField, countcompleteline*countcompleteline*countcompleteline*countcompleteline);
                if curScore > bestScore:
                    bestOffx = offx;
                    bestRot = rot;
                    bestScore = curScore;

                '''
                if countcompleteline > 1 or hasSeenLine:
                    hasSeenLine = True;
                    curScore = self.getReward(completeLineField, countcompleteline*countcompleteline*countcompleteline*countcompleteline);
                    if curScore > bestScore:
                        bestOffx = offx;
                        bestRot = rot;
                        bestScore = curScore;
                    offx = offx+1;
                    self.updateHeightMap(self._game.me.field.field);
                    newField = Field.projectPieceDown(self._game.me.field.field, gamePiece, [offx, 0], self.heightmap);
                    continue;

                for _ in range(len(self._game.nextPiece._rotations)):
                    gamePieceNext = Field.offsetPiece(self._game.nextPiece.positions(), self._game.piecePosition);
                    offxNext = -self.getLeftDist(completeLineField, gamePieceNext);
                    if offxNext > 0:
                        self._game.nextPiece.turnRight();
                        rot = rot+1;
                        continue;

                    self.updateHeightMap(completeLineField);
                    newFieldNext = Field.projectPieceDown(completeLineField, gamePieceNext, [offxNext, 0], self.heightmap);
                    while newFieldNext != None:
                        self.updateHeightMap(newFieldNext);
                        countcompletelineNext, completeLineFieldNext = self.getComplete(newFieldNext);

                        curScore = self.getReward(completeLineFieldNext, countcompleteline*countcompleteline*countcompleteline*countcompleteline + countcompletelineNext*countcompletelineNext*countcompletelineNext*countcompletelineNext);
                        if curScore > bestScore:
                            bestOffx = offx;
                            bestRot = rot;
                            bestScore = curScore;
                        offxNext = offxNext+1;
                        self.updateHeightMap(completeLineField);
                        newFieldNext = Field.projectPieceDown(completeLineField, gamePieceNext, [offxNext, 0], self.heightmap);

                    self._game.nextPiece.turnRight();
                    rotNext = rotNext+1;


                offx = offx+1;
                self.updateHeightMap(self._game.me.field.field);
                newField = Field.projectPieceDown(self._game.me.field.field, gamePiece, [offx, 0], self.heightmap);

            self._game.piece.turnRight();
            rot = rot+1;

        while bestRot > 0:
            moves.append('turnright');
            bestRot = bestRot - 1;

        while bestOffx > 0:
            moves.append('right');
            bestOffx = bestOffx - 1;

        while bestOffx < 0:
            moves.append('left');
            bestOffx = bestOffx + 1;


    def choose(self):

        moves = [];

        self.bfs(moves);
        moves.append('drop');

        return moves
